package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class DatetimeControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_dateMessage_given_json_datetime() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{ \"dateTime\": \"2019-10-01T10:00:00Z\" }"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("\"2019-10-01T10:00:00Z\""));
    }

    @Test
    void should_return_400_dateMessage_given_datejson_notISO_format() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{ \"dateTime\": \"2019-10-01\" }"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

}
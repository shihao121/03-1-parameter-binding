package com.twuc.webApp.controller;

import com.twuc.webApp.entity.Person;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerIntegrationTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void should_return_user_message_given_userId_by_PathVariable() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("this is user find by 1"));
    }

    @Test
    void should_return_userbook_message_given_userId_bookId_by_pathVariable() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/2/books/5"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("this is book 5 belong to user 2"));

    }

    @Test
    void should_return_user_message_given_variable_by_requestParam() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/userName?userName=shihao"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("this is user named shihao"));
    }

    @Test
    void should_return_200_message_given_getUsermessage_with_no_requestParam() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/userName"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("this is user named you-guess"));
    }

    //2.4
    @Test
    void should_return_400_when_given_name_null() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/notnull")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{name}")
        ).andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_200_when_given_name() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/notnull")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"shihao\"}")
        ).andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("shihao"));
    }

    @Test
    void should_return_200_with_message_when_given_name_between_3_9() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/users")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"hahaha\"}"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("hahaha"));
    }

    @Test
    void should_return_404_given_name_not_between_3_9() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/users")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"ha\"}"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_200_with_message_given_user_year_of_birth_between_1000_9999() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/users/info")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"shihao\",\"yearOfBirth\":\"1100\"}"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("shihao is 1100"));
    }

    @Test
    void should_return_400_with_message_given_user_year_of_birth_not_between_1000_9999() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/users/info")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"shihao\",\"yearOfBirth\":\"20\"}"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_200_with_user_message_given_message_contains_email() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/users/info-contains-email")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"shihao\",\"yearOfBirth\":\"1100\",\"email\":\"123@thoughtworks.com\"}"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("shihao email 123@thoughtworks.com 1100"));
    }

    @Test
    void should_return_400_with_user_message_given_message_email_not_format() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/users/info-contains-email")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"shihao\",\"yearOfBirth\":\"1100\",\"email\":\"123thoughtworks.com\"}"))
                .andExpect(MockMvcResultMatchers.status().is(400));

    }

}
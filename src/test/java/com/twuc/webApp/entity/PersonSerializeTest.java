package com.twuc.webApp.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class PersonSerializeTest {

    @Test
    void should_convert_person_object_to_json_String_default_constructor_getter() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String shihao = objectMapper.writeValueAsString(new Person("shihao"));
        assertEquals("{\"name\":\"shihao\"}", shihao);
    }

    @Test
    void should_convert_json_string_to_Person_object() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Person person = objectMapper.readValue("{\"name\":\"shihao\"}", Person.class);
        assertEquals("shihao", person.getName());
    }
}
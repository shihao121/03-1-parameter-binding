package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/books")
public class BookController {

    @GetMapping(value = "/{bookId}")
    public String getBook(@PathVariable int bookId) {
        return String.format("this is books find by %s", bookId);
    }

    @GetMapping(value = "/ids")
    public String getBooks(@RequestParam List<Integer> bookIdList) {
        StringBuilder result = new StringBuilder();
        result.append("this is book ");
        bookIdList.forEach(result::append);
        return result.toString();
    }

}

package com.twuc.webApp.controller;

import com.twuc.webApp.entity.Person;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Size;

@RestController
@RequestMapping(value = "/api/users")
public class UserController {

    @GetMapping(value = "/{userId}")
    public String getUser(@PathVariable Integer userId) {
        return String.format("this is user find by %S", userId);
    }

    @GetMapping(value = "/{userId}/books/{bookId}")
    public String getUserBooks(@PathVariable Integer userId, @PathVariable Integer bookId) {
        return String.format("this is book %s belong to user %s", bookId, userId);
    }

    @GetMapping(value = "/userName")
    public String getUserByName(@RequestParam(defaultValue = "you-guess") String userName) {
        return String.format("this is user named %s", userName);

    }

    @PostMapping(value = "notnull")
    public String getUsername(@RequestBody @Valid Person person) {
        return person.getName();
    }

    @PostMapping()
    public String getUserInfo(@RequestBody @Valid Person person) {
        return person.getName();
    }

    @PostMapping(value = "/info")
    public String getInfoByNameYear(@RequestBody @Valid Person person) {
        return String.format("%s is %d", person.getName(), person.getYearOfBirth());
    }

    @PostMapping(value = "info-contains-email")
    public String getInfoByNameYearEmail(@RequestBody @Valid Person person) {
        return String.format("%s email %s %d", person.getName(), person.getEmail(), person.getYearOfBirth());
    }

}

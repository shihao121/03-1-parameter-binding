package com.twuc.webApp.controller;

import com.twuc.webApp.entity.DateEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;

@RestController
@RequestMapping(value = "/api")
public class DatetimeController {

    @PostMapping(value = "/datetimes")
    public ZonedDateTime getZonedatetime(@RequestBody DateEntity dateTime) {
        return dateTime.getDateTime();
    }



}

package com.twuc.webApp.entity;

import java.time.ZonedDateTime;

public class DateEntity {

    private ZonedDateTime dateTime;

    public DateEntity() {
    }

    public ZonedDateTime getDateTime() {
        return dateTime;
    }
}

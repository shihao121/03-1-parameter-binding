package com.twuc.webApp.entity;

import javax.validation.constraints.*;
import java.io.Serializable;

public class Person{

    @NotNull
    @Size(min = 3, max = 9)
    private String name;

    @Min(1000) @Max(9999)
    private Integer yearOfBirth;

    @Email
    private String email;

    public String getEmail() {
        return email;
    }

    public Person(@NotNull @Size(min = 3, max = 9) String name, @Min(1000) @Max(9999) Integer yearOfBirth, String email) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.email = email;
    }

    public Person(@NotNull @Size(min = 3, max = 9) String name, @Min(1000) @Max(9999) Integer yearOfBirth) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
    }

    public Integer getYearOfBirth() {
        return yearOfBirth;
    }

    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }
}
